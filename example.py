#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ch
import asyncio

class TestBot(ch.RoomManager):
	async def onConnect(self, room):
		print(f"Connected to {room.name}")

	async def onReconnect(self, room):
		print(f"Reconnected to {room.name}")

	async def onDisconnect(self, room):
		print(f"Disconnected from {room.name}")

	async def onMessage(self, room, user, message):
		print(f"[{room.name}] [{user.name}] {message.body}")

		if message.body.lower() == "!a":
			await room.message("AAAAAAAAAAAAAA")

	async def onFloodBan(self, room):
		print(f"You are flood banned in {room.name}")

	async def onPMMessage(self, pm, user, body):
		print(f"[PM] [{user.name}] {body}")

		await pm.message(user, body) # echo

if __name__ == "__main__":
	loop = asyncio.get_event_loop()
	loop.run_until_complete(TestBot.easy_start())
