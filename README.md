# asynch.py
Asyncio version of `ch.py` using the `websockets` library.

### How to install
```bash
$ pip3 install --user -r requirements.txt
```

### License
Distributed under the terms of the GNU GPL.
